package main

import (
	"fmt"
	"os"

	"gitlab.com/s_tomo/open-plus/open"
)

func main() {
	dir, err := open.Open("./cmd")
	fmt.Println(dir)
	if err != nil {
		os.Exit(1)
	} else {
		os.Exit(0)
	}
}
