package open

import (
	"runtime"
	"testing"
)

func TestGetCmd(t *testing.T) {
	c, err := NewContent()
	if err != nil {
		t.Error(err)
	}
	cmd, err := c.GetCmd()
	if err != nil {
		t.Error(err)
	}
	switch runtime.GOOS {
	case "darwin":
		if cmd.Args[0] != "open" {
			t.Fatalf("open != %s", cmd.Args[0])
		}
		t.Logf("open == %s", cmd.Args[0])
	case "windows":
		if cmd.Args[0] != "start" {
			t.Fatalf("start != %s", cmd.Args[0])
		}
		t.Logf("start == %s", cmd.Args[0])
	}
}
