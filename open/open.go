package open

import (
	"errors"
	"os"
	"os/exec"
	"runtime"
)

// Open ファイル, フォルダーを開く
func Open(path string) (string, error) {
	open, err := NewContent()
	if err != nil {
		return open.path, err
	}
	if len(path) > 0 {
		open.SetPath(path)
	}
	cmd, err := open.GetCmd()
	if err != nil {
		return open.path, err
	}
	err = cmd.Run()
	return open.path, err
}

// OpenWith アプリケーションを指定してファイル, フォルダーを開く
func OpenWith(path, appName string) (string, error) {
	open, err := NewContent()
	if err != nil {
		return open.path, err
	}
	// パラメーターの設定
	if len(path) > 0 {
		open.SetPath(path)
	}
	if len(appName) > 0 {
		open.SetAppName(appName)
	}

	cmd, err := open.GetCmd()
	if err != nil {
		return open.path, err
	}

	err = cmd.Run()
	return open.path, err
}

// Content Openコマンドのパラメータ保持
type Content struct {
	path    string
	appNeme string
}

// NewContent 新しいOpenContentを作成する
func NewContent() (*Content, error) {
	path, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	return &Content{path, ""}, nil
}

// IsWindows Windowsかどうか
func (c *Content) IsWindows() bool {
	return runtime.GOOS == "windows"
}

// IsDarwin macOSかどうか
func (c *Content) IsDarwin() bool {
	return runtime.GOOS == "darwin"
}

// SetPath ファイルパスをセットする
func (c *Content) SetPath(path string) {
	c.path = path
}

// SetAppName 起動アプリケーション名をセットする。
func (c *Content) SetAppName(name string) error {
	appName, err := exec.LookPath(name)
	if err != nil {
		return err
	}
	c.appNeme = appName
	return nil
}

// GetCmd exec.Cmd構造体を取得します。
func (c *Content) GetCmd() (cmd *exec.Cmd, err error) {
	var (
		cmdName string
		args    []string
	)
	switch {
	case c.IsWindows():
		// windows
		if len(c.appNeme) > 0 {
			args = append(args, c.appNeme, c.path)
		} else {
			args = append(args, c.path)
		}
		cmdName = "start"
	case c.IsDarwin():
		// macOS
		if len(c.appNeme) > 0 {
			args = append(args, "-a", c.appNeme, c.path)
		} else {
			args = append(args, c.path)
		}
		cmdName = "open"
	default:
		err = errors.New("サポート外の環境です。")
	}
	cmd = exec.Command(cmdName, args...)
	return
}
